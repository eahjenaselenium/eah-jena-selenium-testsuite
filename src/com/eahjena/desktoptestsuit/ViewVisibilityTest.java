package com.eahjena.desktoptestsuit;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.eahjena.pageobject.Views;


public class ViewVisibilityTest extends Lifecycle {
	
	//Oeffnet per Klick alle Elemente, die in der Lage sind, neue Elemente auf der Seite anzuzeigen
	//Test ist nur positiv, wenn erwartes Element anschließend sichtbar wird
	//Soll die Funktionalität aller interaktiven Elemente der Website sicherstellen
	
	@Test
	public void openNavigation() {
		driver.get(mainpage);
		Views views = new Views(driver);
		
		views.showNavigation();
		waitForLoad(driver);
		 
		assertTrue(views.getNavigation().isDisplayed());
		
	}
	
	@Test
	public void openNavigationSubLinks() throws InterruptedException {
		driver.get(mainpage);
		Views views = new Views(driver);

		views.showNavigation();
		views.showSublinks();

		assertTrue(views.getReachedEnd());
	}
	
	@Test
	public void selectImageSlides() throws InterruptedException {
		driver.get(mainpage);
		Views views = new Views(driver);
		
		views.triggerHeaderSlider();

		assertTrue(views.getReachedEnd());
	}
	
	@Test
	public void selectNewsinfoSlides() throws InterruptedException {
		driver.get(mainpage);
		Views views = new Views(driver);
		
		views.triggerNewsInfoSlider();
		
		assertTrue(views.getReachedEnd());
	}
	
	@Test
	public void openInfoContainerSubLinks() throws InterruptedException {
		driver.get(mainpage);
		Views views = new Views(driver);

		views.triggerInfoContainer();
		views.hoverInfoContainerSublinks();
		
		assertTrue(views.getReachedEnd());
		
	}
	
	@Test
	public void selectBackToTop() throws InterruptedException {
		driver.get(mainpage);
		Views views = new Views(driver);
		
		views.scrollToBottom();
		views.selectBackToTopButton();
		
		assertTrue(views.getReachedEnd());
	}


}
