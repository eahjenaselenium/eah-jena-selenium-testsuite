package com.eahjena.desktoptestsuit;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.eahjena.pageobject.LegalPages;

public class LegalPagesTest extends Lifecycle {

	@Test
	public void checkContactDetails() {
		
		driver.get(mainpage);
		LegalPages legalPages = new LegalPages(driver);
		
		legalPages.checkContactDetails();
		
		assertTrue(legalPages.equalsCheckContact());
	}
	
	@Test
	public void checkImportantLinks() {
		
		driver.get(mainpage);
		LegalPages legalPages = new LegalPages(driver);
		
		legalPages.checkLinks();
		
		assertTrue(legalPages.equalsCheckImportantLinks());
	}
	
	@Test
	public void checkDataProtectionContent() {
		
		driver.get("https://www.eah-jena.de/de-de/datenschutz");
		LegalPages legalPages = new LegalPages(driver);
		
		legalPages.checkDataProtectionContact();
		assertTrue(legalPages.equalsCheckDataProtectionContent());
		
		legalPages.checkDataProtectionGoogleAnalytics();
		assertTrue(legalPages.equalsCheckDataProtectionContent());

	}
	
	@Test
	public void checkDataProtectionContentMasterTemplate() {
		
		driver.get("https://www.eah-jena.de/de-de/datenschutz");
		LegalPages legalPages = new LegalPages(driver);
	
		
    	String dataProtectionPath = "resources/PageContent/LegalPages_DataProtection_18.04.2018.txt";
		legalPages.checkLegalInformationAgainstMasterTemplate(dataProtectionPath);
		
		assertTrue(legalPages.equalsCheckLegalInformationAgainstMasterTemplate());
		
	}
	
	@Test
	public void checkLegalInfoContentMasterTemplate() throws Throwable {
		
		driver.get("https://www.eah-jena.de/de-de/impressum");
		LegalPages legalPages = new LegalPages(driver);
	
		
    	String legalInformationPath = "resources/PageContent/LegalPages_LegalInformation_18.04.2018.txt";
		legalPages.checkLegalInformationAgainstMasterTemplate(legalInformationPath);
		
		assertTrue(legalPages.equalsCheckLegalInformationAgainstMasterTemplate());
	}
	
	
}
