package com.eahjena.desktoptestsuit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.eahjena.pageobject.LocatorsFilter;



public class TestStudyCaseFilter extends Lifecycle {
	
	//Test der die Filterfunktion auf https://www.eah-jena.de/de-de/studium/studienangebote testet
	//Ist noch bisschen h�sslich 
	//klickt jede Checkbox einmal an und vergleicht die Anzahl der Ergebnisse mit der tats�chlich angezeigten Zahl
	
	By loadingImage = By.className("loader");
	WebDriverWait wait = new WebDriverWait(driver, 10);
	
	@Test
	@SuppressWarnings("unused")
	public void openFilterPage() {
		
		//Klickpfad bis zur Seite mit Studiengangsfilter 		
		driver.get(mainpage);
		driver.findElement(By.id("showGlobalSideMainMenu")).click();
		driver.findElement(By.xpath("//*[contains(text(), 'Studium')]")).click();
		driver.findElement(By.xpath("//*[contains(text(), 'Studienangebote')]")).click();
					
		//Wartet bis Layer geladen ist
	    wait.until(ExpectedConditions.invisibilityOfElementLocated(loadingImage));

		List <WebElement> NewList = new ArrayList<WebElement>();
		
		LocatorsFilter LocatorsFilter = new LocatorsFilter(driver);
		
		NewList.add(LocatorsFilter.Gesundheit);
		NewList.add(LocatorsFilter.Soziales);
		NewList.add(LocatorsFilter.Technik);
		NewList.add(LocatorsFilter.Wirtschaft);
		NewList.add(LocatorsFilter.InJena);
		NewList.add(LocatorsFilter.AusDerFerne);
		NewList.add(LocatorsFilter.Bachelor);
		NewList.add(LocatorsFilter.Master);
		NewList.add(LocatorsFilter.Sommersemester);
		NewList.add(LocatorsFilter.Wintersemester);
		NewList.add(LocatorsFilter.Vollzeit);
		NewList.add(LocatorsFilter.NebenJob);
		NewList.add(LocatorsFilter.Dual);
		NewList.add(LocatorsFilter.Ja);
		NewList.add(LocatorsFilter.Nein);
			
		int index = 0;
		
		Map<String,Boolean> myMap = new HashMap<String,Boolean>();
		for (WebElement we: NewList) { 
			
		    NewList.get(index).click();
		    
		    int Anzeige = LocatorsFilter.getCountMessage();       
		       System.out.println("Angezeigt werden: " + Anzeige);
		  
		       int Elemente = LocatorsFilter.getCountElements();
		       System.out.println("Tats�chliche Anzahl der Ergebnisse: " +Elemente);
		       
		       String CheckboxName = NewList.get(index).getAttribute("value");
		       if (Anzeige == Elemente)
		    		   {
		    	   			myMap.put(CheckboxName, true);
		    		   }
		       
		       else
		       {
		    	   myMap.put(CheckboxName, false);

		       }
		       
		       boolean value = myMap.get(CheckboxName);
		       if (value != false) {
		          System.out.println("Ergebnisse stimmen �berein");
		       } else {
		           System.out.println("Ergebnisse Stimmen nicht �berein");
		       }
		       
		    NewList.get(index).click();		
		            
	       System.out.println("Diese Element hei�t: " + NewList.get(index).getAttribute("value"));
	       index++;
		}
			
		}
		
	}
	
