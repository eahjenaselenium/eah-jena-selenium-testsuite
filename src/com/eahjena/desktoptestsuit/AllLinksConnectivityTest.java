package com.eahjena.desktoptestsuit;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

//Dieser Test funktioniert etwas anders, als die bisherigen Selenium Tests
//Diese Klasse sammelt nach dem öffnen der Seite alle vorhanden Links, die die angegebene Bedingung erfüllen
//Dannach wird ein Request abgeschickt und der HTTP Response Status Code abgefangen
//In der Konsole werden die Ergebnisse angezeigt, sobald der Test fertig ist
//Die Eah-Seite gestaltet sich hier als problematisch, da die Standard HttpUrlConnection Klasse
//aus Securitygründen keine Redirects zwischen Protokollen von HTTP und HTTPS automatisch zu lässt
//Aus dem Grund musste ein Workaround gebaut werden, der manuel die Location Tags sammelt und solange neu abfragt
//bis alle Redirects eingesammelt wurden. Anzumerken ist, dass die Eah-Seite sehr viele verschiedene Redirects hat.
//Dazu bitte den Logverlauf und die Ausgabe in der Konsole betrachten fuer naehere Informationen.

public class AllLinksConnectivityTest extends Lifecycle {

	private String url = "";
	private String eah = "eah-jena"; 										// Bedingung: Link wird aufgerufen, wenn "eah-jena" im Link vorkommt
	private String skipUrl = "URL belongs to another domain, skipping it."; // Konsolenanzeige fuer Testergebnis
	private String fileUrl = "URL points on a file, skipping it.";
	private String divider = "\t#################### ";

	Map<Integer, Integer> statusMap = new HashMap<Integer, Integer>();

	URL resourceUrl, base, next;
	HttpURLConnection huc;
	String location;

	@Test
	public void checkAllLinksConnectivity() throws MalformedURLException, InterruptedException {
		driver.get(mainpage);												// Mit diesem Test kann jede Seite angesteuert werden, einfach den link hier eintragen
																			// Zusätzlich muss das selbe in der Zeile "if (!url.contains(eah))" eingetragen werden anstelle von "eah"
		HttpURLConnection huc = null;

		int respCode = 200;
		String respMessage;

		waitForLoad(driver);

		List<WebElement> links = driver.findElements(By.tagName("a")); 		// Identifiziert alle Links und sammelt sie in
																			// Liste

		Iterator<WebElement> it = links.iterator(); 						// Iterator fuer Links liste

		while (it.hasNext()) {

			url = it.next().getAttribute("href");

			if (url == null || url.isEmpty()) { 							// Testet, ob Link null oder leer ist, überspringt diesen dann
				continue;
			}

			if (!url.contains(eah)) { 										// Testet, ob aufgerufene URL Teil der übergebenen Bedingung (eah-jena) oder die Seite
																			// eines dritten ist
				System.out.println();
				System.out.println("Request URL ... " + url);
				System.out.println("Response Code ... " + skipUrl);
				continue;
			}

			if (url.endsWith(".pdf")) {										// Testet, ob aufgerufene URL eine Datei ist, überspringt diese dann
				System.out.println();
				System.out.println("Request URL ... " + url);
				System.out.println("Response Code ... " + fileUrl);
				continue;
			}

			try {
				boolean redirect = false;

				huc = (HttpURLConnection) (new URL(url).openConnection()); 	// Methode, um HTTP-Requests zu senden
				huc.setRequestMethod("HEAD"); 								// Request-Typ kann auf "GET" oder "HEAD" gestellt werden, um nur Header
																			// und keine Bodys zu pruefen
				huc.connect();
				huc.setConnectTimeout(15000);
				huc.setReadTimeout(15000);
				huc.setInstanceFollowRedirects(false); 						// Muss hier auf false gesetzt werden, damit die manuelle
																			// Weiterleitung unten funktioniert. Hier wird kein Redirect von
																			// Http zu Https unterstuetzt

				respCode = huc.getResponseCode();
				respMessage = huc.getResponseMessage();

				System.out.println();
				System.out.println("Request URL ... " + url);

				if (respCode != HttpURLConnection.HTTP_OK) {
					if (respCode == HttpURLConnection.HTTP_MOVED_TEMP || respCode == HttpURLConnection.HTTP_MOVED_TEMP
							|| respCode == HttpURLConnection.HTTP_MOVED_PERM
							|| respCode == HttpURLConnection.HTTP_SEE_OTHER)
						redirect = true;									// Setzt einen Flag für alle Seiten mit einem Response Code im 3xx Bereich
				}

				System.out.println("Response Code ... " + respCode + " " + respMessage);
				store(respCode);

				if (redirect) {

					int secondRespCode;

					do {
						String newUrl = huc.getHeaderField("Location");

						huc = (HttpURLConnection) new URL(newUrl).openConnection();

						secondRespCode = huc.getResponseCode();
						String secondRespMessage = huc.getResponseMessage();

						System.out.println("\t\tRedirect to URL ... " + newUrl);
						System.out.println("\t\tNew StatusCode ... " + secondRespCode + " " + secondRespMessage);

						store(secondRespCode);

					} while (secondRespCode > 300 || secondRespCode < 400);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
			}
		}

		list();
	}

	
	
	// Hilfsmethode, die in der Testmethode aufgerufen wird. Speichert alle Endergebnisse in einer Map, um diese am Ende für die
	// Logs ausgeben zu können
	
	public void store(int code) {
		if (statusMap.containsKey(code)) {
			int value = statusMap.get(code);
			statusMap.put(code, value + 1);
		} else {
			statusMap.put(code, 1);
		}
	}

	
	// Hilfsmethode, die am Ende des Testes eine Liste aller Ergebnisse zusammen mit ihren Bezeichnungen ausgibt
	
	public void list() {
		String crit = null;
		String msg = null;
		System.out.println();

		Iterator<Integer> iter = statusMap.keySet().iterator();
		while (iter.hasNext()) {
			int code = iter.next();

			switch (code) {
			case 200:msg = "OK";crit = " ";break;
			case 300:msg = "Multiple Choices";crit = " ";break;
			case 301:msg = "Moved Permanently";crit = " ";break;
			case 302:msg = "Moved Temporarily";crit = " ";break;
			case 303:msg = "See Other";crit = " ";break;
			case 400:msg = "Bad Request";crit = " ";break;
			case 401:msg = "Unauthorized";crit = " ";break;
			case 402:msg = "Payment Required";crit = " ";break;
			case 403:msg = "Forbidden";crit = " || CRITICAL RESPONSE";break;
			case 404:msg = "Not Found";crit = " || CRITICAL RESPONSE";break;
			default:msg = "Not defined";
			}

			LOGGER.info(divider + statusMap.get(code) + "x \t:\t" + code + " " + msg + crit);
		}
		LOGGER.info("SCROLL UP TO FIND MORE INFORMATION ON CRITICAL RESPONSES ");
	}
}
