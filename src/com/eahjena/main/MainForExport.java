package com.eahjena.main;

import org.junit.internal.TextListener;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

import com.eahjena.desktoptestsuit.*;


public class MainForExport {

	
	//Main()-Methode wird benötigt, um Projekt als Datei zu exportieren und anschließend auszuführen
	//
	//#1:	In "com.eahjena.testsuit/Livecycle.java" muss noch der passende Selenium Driver ausgewählt werden
	//#2:	Dabei muss beruecksichtigt werden, ob die Anwendung auf Windows oder Linux ausgefuehrt wird (siehe Kommentare)
	//#3:	Eclipse: File >> Export >> Java >> Runnable JAR File
	//#4:	Der Ordner "driver" muss in das selbe Verzeichnis, wie die exportierte .jar kopiert werden
	//		Projekt Explorer: Rechtsklick auf driver >> Show in: >> System Explorer
	//#5:	Datei kann dann ueber Konsole mit "java -jar filename.jar" ausgefuehrt werden
		
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		JUnitCore junit = new JUnitCore();
		junit.addListener(new TextListener(System.out));
		Result result = junit.run(AllTests.class);			//AllTests.class verweist Klasse, die alle Tests zusammen fasst
		if (result.getFailureCount() > 0) {
			System.out.println("Test failed.");
			System.exit(1);
		} else {
			System.out.println("Test finished successfully.");
			System.exit(0);
		}
	}

}
