package com.eahjena.main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

		/*
		 * This class creates a snapshot of the current page content. 
		 * The files directory is in the resource/PageContent folder of your project (type is .txt).
		 * This file will serve as a master template to let the "LegalPagesTest.java" - test fail, 
		 * if the content of the mainpage differs somehow from the mastertemplate. 
		 * Edit "driver.get(x)" variable to decide which page should be opened, before you start!
		 */


public class TextCreator {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "driver/chromedriver-windows.exe");				 // Chrome Driver fuer Windows

		Map<String, String> mobileEmulation = new HashMap<>();
		mobileEmulation.put("deviceName", "Pixel 2");

		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);

		WebDriver driver = new ChromeDriver(chromeOptions);
		
//		WebDriver driver = new ChromeDriver();

		String idForTxtFile = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
		File legalInformationFile = new File("resources/PageContent/LegalPages_LegalInformation_" + idForTxtFile + ".txt");
		File dataProtectionFile = new File("resources/PageContent/LegalPages_DataProtection_" + idForTxtFile + ".txt");

		String dataProtectionUrl = "https://www.eah-jena.de/de-de/datenschutz";
		String legalInformationUrl = "https://www.eah-jena.de/de-de/impressum";
		
		/*
		 * Choose here the page from which you want to save a snapshot in the
		 * coresponding textfile. You can choose between the Legal Information page and
		 * Data Protection Page. The strings are right above.
		 */
			driver.get(legalInformationUrl);

			
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		WebElement legalPageContent = driver.findElement(By.id("page-content"));

		// Filewriter
		try {
			
			// TODO Switch case, set default file for default case. Will be usable this way for every page passed in driver.get
			
			if (driver.getCurrentUrl().equals(dataProtectionUrl)) {

				FileWriter fw = new FileWriter(dataProtectionFile, false);

				fw.write(legalPageContent.getText());
				fw.flush();
				fw.close();
			};
			
			if (driver.getCurrentUrl().equals(legalInformationUrl)) {

				FileWriter fw = new FileWriter(legalInformationFile, false);

				fw.write(legalPageContent.getText());
				fw.flush();
				fw.close();
			};

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.manage().deleteAllCookies();
		driver.close();
	}

}