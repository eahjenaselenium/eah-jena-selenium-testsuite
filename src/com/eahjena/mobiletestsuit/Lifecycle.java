package com.eahjena.mobiletestsuit;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Lifecycle {
	
	//Diese Klasse dient dazu, alle bisherigen Tests zu erweitern. Hier werden sämtliche Methoden festgehalten,
	//die vor, während und nach jedem Tests ausgeführt werden müssen. 
	
	final String mainpage = "https://www.eah-jena.de/";
	final String endline = "########################################################################";
	protected static WebDriver driver;
	protected static Logger LOGGER = null;
	
    @Rule
    public TestWatcher watchman = new TestWatcher() {
    	@Override
    	protected void starting(Description description) {
    		LOGGER.info("INVOKED TESTCASE" + " [" + description + "]");
    		
    	}
        @Override
        protected void failed(Throwable e, Description description) {
            LOGGER.error("FAILED " + description, e);
        }
 
        @Override
        protected void succeeded(Description description) {
            LOGGER.info("SUCCESS " + endline);
        }
    };
        
	
	@BeforeClass
	public static void setUp() {
		
			System.setProperty("webdriver.chrome.driver","driver/chromedriver-windows.exe");

			Map<String, String> mobileEmulation = new HashMap<>();
			mobileEmulation.put("deviceName", "Pixel 2");

			ChromeOptions chromeOptions = new ChromeOptions();
			chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);

			driver = new ChromeDriver(chromeOptions);
			driver.manage().window().maximize();
		
		System.setProperty("log4j.configurationFile","log4j2.xml");							//ConfigFile fuer Logs
		LOGGER = LogManager.getLogger();													//Initialisiert Logger
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
	}

	@After
	public void cleanUp(){
		driver.manage().deleteAllCookies();
	}
	
	@AfterClass
	public static void tearDown(){
		driver.close();
	}	
	
	//Hilfsklasse, die wartet, bis der komplette Seiteninhalt gelanden ist (zusammen mit allen JavaScript Inhalten)
	//Muss nicht initialisiert werden, kann mit waitForLoad(driver) in allen Testklassen aufgerufen werden, die enthalten:
	//public class TestcaseName extends Lifecycle
	
	public void waitForLoad(WebDriver driver) {
		LOGGER.info("waiting for javascript to finish");
	    new WebDriverWait(driver, 30).until((ExpectedCondition<Boolean>) wd ->
	            ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete")&&((Boolean)((JavascriptExecutor)driver).executeScript("return jQuery.active == 0")));
	}
}
