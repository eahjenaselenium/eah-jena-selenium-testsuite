package com.eahjena.mobiletestsuit;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.eahjena.pageobject.Searchbar;
import com.eahjena.pageobject.Searchresult;

public class SearchFunctionalityTest extends Lifecycle {
	
	@Test
	//aktiviert die Suchleiste
	public void openSearchbar() {
		driver.get(mainpage);
		Searchbar searchbar = new Searchbar(driver);
		
		searchbar.showSearchbar();
		searchbar.selectSearchInput();
		
		//Test positiv, wenn Suchleiste angezeigt wird
		assertTrue(searchbar.getSearchInputField().isDisplayed());
	}

	@Test
	//sucht nach leerem Input
	public void searchEmptyKeyword() throws InterruptedException {
		driver.get(mainpage);
		Searchbar searchbar = new Searchbar(driver);
		Searchresult searchresult = new Searchresult(driver);
		
		String searchterm = " ";
		
		searchbar.showSearchbar();
		searchbar.selectSearchInput();
		searchbar.sendSearchTerm(searchterm);
		
		//Test positiv, wenn "Kein Ergebnis" - Seite auftaucht
		assertTrue(searchresult.noResultFound());
	}
	
	@Test
	//sucht nach "Prof Erfurth"
	public void searchExistendKeyword1() throws InterruptedException {
		driver.get(mainpage);
		Searchbar searchbar = new Searchbar(driver);
		Searchresult searchresult = new Searchresult(driver);
		
		String st1 = "Prof";					//Suchbegriff 1
		String st2 = "Erfurth";					//Suchbegriff 2
		String stSpace = " ";					//String Leerzeichen
		String stRandomChar = "(.*)";			//Platzhalter fuer Methode .matches() die ein zufaelliges Zeichen darstellt
		
		searchbar.showSearchbar();
		searchbar.selectSearchInput();
		waitForLoad(driver);
		searchbar.sendSearchTerm(st1 + stSpace + st2);
		waitForLoad(driver);
		//Test positiv, wenn im ersten Suchergebnis Suchbegriff[st2] vorhanden ist
		assertTrue(searchresult.getSearchItemTitle().getText().matches(stRandomChar + st2 + stRandomChar));		
	}
	
	@Test
	//sucht nach "Prof Werner"
	public void searchExistendKeyword2() throws InterruptedException {
		driver.get(mainpage);
		Searchbar searchbar = new Searchbar(driver);
		Searchresult searchresult = new Searchresult(driver);
		
		String st1 = "Prof";					//Suchbegriff 1
		String st2 = "Werner";					//Suchbegriff 2
		String stSpace = " ";					//String Leerzeichen
		String stRandomChar = "(.*)";			//Platzhalter fuer Methode .matches() die ein zufälliges Zeichen darstellt
		
		searchbar.showSearchbar();
		searchbar.selectSearchInput();
		waitForLoad(driver);
		searchbar.sendSearchTerm(st1 + stSpace + st2);
		waitForLoad(driver);
		//Test positiv, wenn im ersten Suchergebnis Suchbegriff[st2] vorhanden ist
		assertTrue(searchresult.getSearchItemTitle().getText().matches(stRandomChar + st2 + stRandomChar));		
	}
}