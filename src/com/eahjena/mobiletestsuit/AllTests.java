package com.eahjena.mobiletestsuit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

//Diese Klasse fasst alle Tests fuer "com.eahjena.main/MainForExport.java" zusammen
//Die Namen der Tests muessen alle in @SuiteClasses eingetragen oder automatisiert erstellt werden
//Diese Klasse kann automatisiert erstellt werden, sobald alle Tests fertig geschrieben sind
//Dazu Rechtsklick auf "com.eahjena.testsuit" >> new >> other >> Java >> JUnit >> JUnit Test Suite

@RunWith(Suite.class)
@SuiteClasses({ 
	SearchFunctionalityTest.class, 
	ViewVisibilityTest.class, 
	TestStudyCaseFilter.class, 
	DepartmentTest.class, 
	LegalPagesTest.class
	})
public class AllTests {

}
