package com.eahjena.mobiletestsuit;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.ui.WebDriverWait;

import com.eahjena.pageobject.LocatorsDepartments;


//Test for departements of the university

public class DepartmentTest extends Lifecycle {
	

	By loadingImage = By.className("loader");
	WebDriverWait wait = new WebDriverWait(driver, 10);
	
	@Test
	public void openFilterPage() {
				
		driver.get(mainpage);
		
		
		WebElement pageBottom = driver.findElement(By.tagName("footer"));
		pageBottom.click();
		
		//Test if all departements are on EAH-Website
		WebElement BW = driver.findElement(By.xpath("//a[@title='"+LocatorsDepartments.BW+"']"));
		WebElement ETundIT = driver.findElement(By.xpath("//a[@title='"+LocatorsDepartments.ETundIT+"']"));
		WebElement GesundPf = driver.findElement(By.xpath("//a[@title='"+LocatorsDepartments.GesUndPf+"']"));
		WebElement Grundl =driver.findElement(By.xpath("//a[@title='"+LocatorsDepartments.Grundl+"']"));
		WebElement MA = driver.findElement(By.xpath("//a[@title='"+LocatorsDepartments.MA+"']"));
		WebElement MediUndBio = driver.findElement(By.xpath("//a[@title='"+LocatorsDepartments.MedinUndBio+"']"));
		WebElement SciTec = driver.findElement(By.xpath("//a[@title='"+LocatorsDepartments.SciTec+"']"));
		WebElement Sozialwesen= driver.findElement(By.xpath("//a[@title='"+LocatorsDepartments.Sozi+"']"));
		WebElement WI = driver.findElement(By.xpath("//a[@title='"+LocatorsDepartments.Wi+"']"));
		System.out.println("All departements are listed in footer of EAH-jena.de");
		
		
		//Test if current Links of departments equals Links at the status of creation of the test
		Boolean BoolBw = BW.getAttribute("href").equals(LocatorsDepartments.LinkBetriebswirtschaft);
		Boolean BoolETundIT = ETundIT.getAttribute("href").equals(LocatorsDepartments.LinkElektroUndInformationstechnik);
		Boolean BoolGesUndPflege = GesundPf.getAttribute("href").equals(LocatorsDepartments.LinkGesundheitUndPflege);
		Boolean BoolGrundlagen = Grundl.getAttribute("href").equals(LocatorsDepartments.LinkGrundlagenwissenschaften);
		Boolean BoolMaschinenbau = MA.getAttribute("href").equals(LocatorsDepartments.LinkMaschinenbau);
		Boolean BoolMedizinUndBio = MediUndBio.getAttribute("href").equals(LocatorsDepartments.LinkMedizinUndBiotechnologie);   
		Boolean BoolSciTec = SciTec.getAttribute("href").equals(LocatorsDepartments.LinkSciTec);
		Boolean BoolSozi = Sozialwesen.getAttribute("href").equals(LocatorsDepartments.LinkSozialwesen);
		Boolean BoolWi = WI.getAttribute("href").equals(LocatorsDepartments.LinkWirtschaftsingenieurwesen);
		
		System.out.println("VERGLEICH DER AKTUELLEN LINKS MIT DEN HINTERLEGTEN LINKS");
		System.out.println("");
		System.out.println("Betriebswirtschaft: "+ BoolBw);   
		System.out.println("Elektrotechnik und Informationstechnik: "+ BoolETundIT);
		System.out.println("Gesundheit und Pflege: "+ BoolGesUndPflege);    
		System.out.println("Grundlagenwissenschaften: "+ BoolGrundlagen);
		System.out.println("Maschinenbau: "+ BoolMaschinenbau);    
		System.out.println("Medizintechnik und Biotechnologie: "+ BoolMedizinUndBio);  
		System.out.println("SciTec: "+ BoolSciTec);    
		System.out.println("Sozialwesen: "+ BoolSozi);   
		System.out.println("Wirtschaftsingenieurwesen: "+ BoolWi);
		
		
		assertTrue("Links stimmen nicht �berein f�r den Bereich " + LocatorsDepartments.BW, BoolBw);
		assertTrue("Links stimmen nicht �berein f�r den Bereich "+ LocatorsDepartments.ETundIT, BoolETundIT);
		assertTrue("Links stimmen nicht �berein f�r den Bereich " + LocatorsDepartments.GesUndPf, BoolGesUndPflege);
		assertTrue("Links stimmen nicht �berein f�r den Bereich " + LocatorsDepartments.Grundl, BoolGrundlagen);
		assertTrue("Links stimmen nicht �berein f�r den Bereich " + LocatorsDepartments.MA, BoolMaschinenbau);
		assertTrue("Links stimmen nicht �berein f�r den Bereich " + LocatorsDepartments.MedinUndBio, BoolMedizinUndBio);
		assertTrue("Links stimmen nicht �berein f�r den Bereich " + LocatorsDepartments.SciTec, BoolSciTec);
		assertTrue("Links stimmen nicht �berein f�r den Bereich " + LocatorsDepartments.Sozi, BoolSozi);
		assertTrue("Links stimmen nicht �berein f�r den Bereich " + LocatorsDepartments.Wi, BoolWi);
		
		System.out.println("All Links match with the links of the status of creation of this test");
		
	}
}
	
