package com.eahjena.pageobject;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;



public class Searchbar extends PageObject {
	
	@FindBy(id="showhideSearchbar")
	private WebElement showhideSearch;
	
	@FindBy(id="ctl00_SmallSearchInputBox1_csr_sbox")
	private WebElement searchInputField;

	@FindBy(id="ctl00_SmallSearchInputBox1_csr_SearchLink")
	private WebElement searchEnterButton;

	public WebElement getSearchInputField() {
		return searchInputField;
	}

	public void showSearchbar() {
		waitClickable(showhideSearch);
		LOG.info("click on searchbar icon");
		showhideSearch.click();		
	}
	
	public void selectSearchInput() {
		waitClickable(searchInputField);
		LOG.info("select search input field");
		searchInputField.click();
	}
	
	public void sendSearchTerm(String term) throws InterruptedException {
		LOG.info("clear input in field");
		this.searchInputField.clear();
		LOG.info("send predefined keys: '" + term + "'");
		this.searchInputField.sendKeys(term);
		LOG.info("select search button");
		searchEnterButton.click();
		Thread.sleep(2000);
	}
	
	
	//Methode, die max. 10sek wartet, bis uebergebenes Webelement clickable ist
	public void waitClickable(WebElement var) {
		LOG.info("wait till Webelement is clickable");
		WebDriverWait wait = new WebDriverWait(driver, 10);
		@SuppressWarnings("unused")
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(var));
	}
	
	//Constructor
	public Searchbar(WebDriver driver) {
		super(driver);
	}
	
}