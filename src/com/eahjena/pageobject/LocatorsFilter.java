package com.eahjena.pageobject;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;



public class LocatorsFilter extends PageObject {
	
	public LocatorsFilter(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	public void OpenStudium()
	{
		driver.findElement(By.xpath("//*[contains(text(), 'Studium')]")).click();
	}
	
	public void OpenStudienangebote()
	{
		driver.findElement(By.xpath("//*[contains(text(), 'Studienangebote')]"));
	}
	
//	//Navigation
//	public WebElement Studium = driver.findElement(By.xpath("//*[contains(text(), 'Studium')]"));
//	public WebElement Studienangebote = driver.findElement(By.xpath("//*[contains(text(), 'Studienangebote')]"));
	
	//Gesamtanzahl der Suchergebnisse
	public WebElement MajorCount= driver.findElement(By.className("major-count"));
	
	//Checkboxes 
	public WebElement Gesundheit = driver.findElement(By.xpath("//input[@value='Gesundheit'and @type='checkbox']"));
	public WebElement Soziales = driver.findElement(By.xpath("//input[@value='Soziales'and @type='checkbox']"));
	public WebElement Technik = driver.findElement(By.xpath("//input[@value='Technik'and @type='checkbox']"));
	public WebElement Wirtschaft = driver.findElement(By.xpath("//input[@value='Wirtschaft'and @type='checkbox']"));
	public WebElement InJena = driver.findElement(By.xpath("//input[@value='in Jena'and @type='checkbox']"));
	public WebElement AusDerFerne = driver.findElement(By.xpath("//input[@value='aus der Ferne'and @type='checkbox']"));
	public WebElement Bachelor= driver.findElement(By.xpath("//input[@value='Bachelor'and @type='checkbox']"));
	public WebElement Master = driver.findElement(By.xpath("//input[@value='Master'and @type='checkbox']"));
	public WebElement Sommersemester = driver.findElement(By.xpath("//input[@value='Sommersemester'and @type='checkbox']"));
	public WebElement Wintersemester = driver.findElement(By.xpath("//input[@value='Wintersemester'and @type='checkbox']"));
	public WebElement Vollzeit = driver.findElement(By.xpath("//input[@value='Vollzeit studieren'and @type='checkbox']"));
	public WebElement NebenJob = driver.findElement(By.xpath("//input[@value='neben dem Job studieren'and @type='checkbox']"));
	public WebElement Dual = driver.findElement(By.xpath("//input[@value='Dual studieren'and @type='checkbox']"));
	public WebElement Ja = driver.findElement(By.xpath("//input[@value='Ja'and @type='checkbox']"));
	public WebElement Nein = driver.findElement(By.xpath("//input[@value='Nein'and @type='checkbox']"));
		

	public int getCountMessage()
	{
		return Integer.parseInt(MajorCount.getText());
		
	}
	
	
	public int getCountElements()
	{
			List <WebElement> optionCount = driver.findElements(By.xpath("//span[@class='eah-major-tile']"));
			//1 redundantes Element (deswegen -1)
			return optionCount.size()-1 ;
	}
	
	
}