package com.eahjena.pageobject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LegalPages extends PageObject {

	// Contact details of mainpage
	
	@FindBy(className = "uni-title")
	private WebElement titleEle;

	@FindBy(className = "uni-subtitle")
	private WebElement subTitleEle;

	@FindBy(className = "street")
	private WebElement streetEle;

	@FindBy(className = "zip")
	private WebElement postCodeEle;

	@FindBy(className = "tel")
	private WebElement phoneEle;
	
	
	// Find links by partial link text for verification of legal pages
	
	@FindBy(partialLinkText = "Impressum")
	private WebElement legalInfoLink;

	@FindBy(partialLinkText = "Datenschutz")
	private WebElement dataProtectionLink;

	
	// Data protection page - contact details 
	
	@FindBy(className = "contact-name")
	private WebElement contactNameEle;
	
	@FindBy(className = "contact-jobtitle")
	private WebElement contactJobtitleEle;
	
	@FindBy(className = "contact-workphone")
	private WebElement contactWorkphoneEle;
	
	@FindBy(className = "contact-workfax")
	private WebElement contactWorkfaxEle;
	
	@FindBy(className = "contact-email")
	private WebElement contactEmailEle;	
	
	@FindBy(className = "contact-roomnumber")
	private WebElement contactRoomnumberEle;	

	
	// Data protection page - google analytics
	
	@FindBy(xpath = "//*[contains(text(), 'Google Analytics')]")
	private WebElement googleAnalyticsEle;
	
	
	// Legal info page - important titles

	@FindBy(xpath = "//*[contains(text(), 'Einrichtung​​')]")
	private WebElement facilityEle;
	
	@FindBy(xpath = "//*[contains(text(), 'Herausgeber')]")
	private WebElement editorEle;
	
	@FindBy(xpath = "//*[contains(text(), 'Redaktion')]")
	private WebElement editorialEle;
	
	@FindBy(xpath = "//*[contains(text(), 'Haftungsausschluss')]")
	private WebElement disclaimerEle;

	
	/* Mainpage University - Contact Details
	 * Snapshot of actual data from mainpage. Includes Contact details of university.
	 * Tests will fail if Page content changes from the data given below.
	 */
	
	final private String title = "Ernst-Abbe-Hochschule Jena";
	final private String subTitle = "University of Applied Sciences";
	final private String street = "Carl-Zeiss-Promenade 2";
	final private String postCode = "07745 Jena";
	final private String phone = "+49 (0) 3641 / 205 0";
	
	
	/* Mainpage University - Important links
	 * Snapshot of Important links from the Mainpage that shouldn't be changed and needed to be always
	 * displayed, to avoid legal problems and overall confusion.
	 * Tests will fail if Page content changes from the data given below.
	 */
	
	final private String dataProtection = "http://www.eah-jena.de/de-de/datenschutz";
	final private String legalInfo = "http://www.eah-jena.de/de-de/impressum";
	
	
	/* DataProtection Page - Contact Details
	 * Snapshot of actual data from DataProtection Page. Includes contact details of contact
	 * in case of interest and additionally titels of important content areas.
	 * Tests will fail if Page content changes from the data given below.
	 */
	
	final private String contactName = "Dr. Carsten Morgenroth";
	final private String contactJobtitle = "Justiziar, Vertreter des Kanzlers";
	final private String contactWorkphone = "+49 3641 205 210";
	final private String contactWorkfax = "+49 3641 205 201";
	final private String contactEmail = "carsten.morgenroth@eah-jena.de";
	final private String contactRoomnumber = "01.04.14";
	final private String googleAnalytics = "Google Analytics";
	
	
	/* LegalInfo Page - Important Titles
	 * Snapshot of actual data from DataProtection Page. Includes contact details of contact
	 * in case of interest and additionally titels of important content areas.
	 * Tests will fail if Page content changes from the data given below.
	 */
	

	

	
	
	boolean booleanHelper = false;

	public void checkContactDetails() {
		LOG.info("checks if contact details are fully displayed and have the same content as before");
		if (titleEle.getText().equalsIgnoreCase(title)) {
			LOG.info("Field contains: " + titleEle.getText());
			LOG.info("Field expected: " + title);
			LOG.info("####### SUCCESS #######");
			
			if (subTitleEle.getText().equalsIgnoreCase(subTitle)) {
				LOG.info("Field contains: " + subTitleEle.getText());
				LOG.info("Field expected: " + subTitle);
				LOG.info("####### SUCCESS #######");
				
				if (streetEle.getText().equalsIgnoreCase(street)) {
					LOG.info("Field contains: " + streetEle.getText());
					LOG.info("Field expected: " + street);
					LOG.info("####### SUCCESS #######");
					
					if (postCodeEle.getText().equalsIgnoreCase(postCode)) {
						LOG.info("Field contains: " + postCodeEle.getText());
						LOG.info("Field expected: " + postCode);
						LOG.info("####### SUCCESS #######");
						
						if (phoneEle.getText().contains(phone)) {
							LOG.info("Field contains: " + phoneEle.getText());
							LOG.info("Field expected: " + phone);
							LOG.info("####### SUCCESS #######");
							booleanHelper = true;
						} else {
							LOG.info("Test failed at: " + phoneEle.getText());
							LOG.info("XXXXXXX FAILED  XXXXXXX");							
						} 
					} else {
						LOG.info("Test failed at: " + postCodeEle.getText());
						LOG.info("XXXXXXX FAILED  XXXXXXX");	
					}
				} else {
					LOG.info("Test failed at: " + streetEle.getText());
					LOG.info("XXXXXXX FAILED  XXXXXXX");	
				}
			} else {
				LOG.info("Test failed at: " + subTitleEle.getText());
				LOG.info("XXXXXXX FAILED  XXXXXXX");	
			}
		} else {
			LOG.info("Test failed at: " + titleEle.getText());
			LOG.info("XXXXXXX FAILED  XXXXXXX");	
		}
	}
	
	public void checkLinks() {

		if(legalInfoLink.getAttribute("href").equals(legalInfo)) {
			LOG.info("Field contains: " + legalInfoLink.getAttribute("href"));
			LOG.info("Field expected: " + legalInfo);
			LOG.info("####### SUCCESS #######");
			
			if(dataProtectionLink.getAttribute("href").equals(dataProtection)) {
				LOG.info("Field contains: " + dataProtectionLink.getAttribute("href"));
				LOG.info("Field expected: " + dataProtection);
				LOG.info("####### SUCCESS #######");
				
				booleanHelper = true;
			} else {
				LOG.info("Test failed at: " + dataProtectionLink.getAttribute("href"));
				LOG.info("XXXXXXX FAILED  XXXXXXX");	
			}
		} else {
			LOG.info("Test failed at: " + legalInfoLink.getAttribute("href"));
			LOG.info("XXXXXXX FAILED  XXXXXXX");	
		}
	}
	
	public void checkDataProtectionContact() {
		if (contactNameEle.getText().equals(contactName)) {
			LOG.info("Field contains: " + contactNameEle.getText());
			LOG.info("Field expected: " + contactName);
			LOG.info("####### SUCCESS #######");
			
			if (contactJobtitleEle.getText().equals(contactJobtitle)) {
				LOG.info("Field contains: " + contactJobtitleEle.getText());
				LOG.info("Field expected: " + contactJobtitle);
				LOG.info("####### SUCCESS #######");
				
				if (contactWorkphoneEle.getText().equals(contactWorkphone)) {
					LOG.info("Field contains: " + contactWorkphoneEle.getText());
					LOG.info("Field expected: " + contactWorkphone);
					LOG.info("####### SUCCESS #######");
					
					if (contactWorkfaxEle.getText().equals(contactWorkfax)) {
						LOG.info("Field contains: " + contactWorkfaxEle.getText());
						LOG.info("Field expected: " + contactWorkfax);
						LOG.info("####### SUCCESS #######");
						
						if(contactEmailEle.getText().equals(contactEmail)) {
							LOG.info("Field contains: " + contactEmailEle.getText());
							LOG.info("Field expected: " + contactEmail);
							LOG.info("####### SUCCESS #######");
							
							if(contactRoomnumberEle.getText().equals(contactRoomnumber)) {
								LOG.info("Field contains: " + contactRoomnumberEle.getText());
								LOG.info("Field expected: " + contactRoomnumber);
								LOG.info("####### SUCCESS #######");
								
								booleanHelper = true;
								
							} else {
								LOG.info("Test failed at: " + contactRoomnumberEle.getText());
								LOG.info("XXXXXXX FAILED  XXXXXXX");
								booleanHelper = false;
							}
							
						} else {
							LOG.info("Test failed at: " + contactEmailEle.getText());
							LOG.info("XXXXXXX FAILED  XXXXXXX");
							booleanHelper = false;
						}
						
					} else {
						LOG.info("Test failed at: " + contactWorkfaxEle.getText());
						LOG.info("XXXXXXX FAILED  XXXXXXX");
						booleanHelper = false;
					}
					
				} else {
					LOG.info("Test failed at: " + contactWorkphoneEle.getText());
					LOG.info("XXXXXXX FAILED  XXXXXXX");
					booleanHelper = false;
				}
			} else {
				LOG.info("Test failed at: " + contactJobtitleEle.getText());
				LOG.info("XXXXXXX FAILED  XXXXXXX");	
				booleanHelper = false;
			}
		} else {
			LOG.info("Test failed at: " + contactNameEle.getText());
			LOG.info("XXXXXXX FAILED  XXXXXXX");	
			booleanHelper = false;
		}
		
	}
	
	public void checkDataProtectionGoogleAnalytics() {
		LOG.info("Checks if specific content areas are mentioned in data protection page");
		if (googleAnalyticsEle.getText().equalsIgnoreCase(googleAnalytics)) {
			LOG.info("Field contains: " + googleAnalyticsEle.getText());
			LOG.info("Field expected: " + googleAnalytics);
			LOG.info("####### SUCCESS #######");
			booleanHelper = true;
			
		} else {
			LOG.info("Test failed at: " + googleAnalyticsEle.getText());
			LOG.info("XXXXXXX FAILED  XXXXXXX");	
			booleanHelper = false;
		}
		
	}

	private static String readTextFile(String filePath)
	{
		LOG.info("Initialize - Read Master Text File Method - ");
	    String content = "";
	    try
	    {	
	    	LOG.info("Reading File in " + filePath);
	        content = new String ( Files.readAllBytes( Paths.get(filePath) ) );
	    }
	    catch (IOException e)
	    {
	        e.printStackTrace();
	    }
	    return content;
	}
	
    public void checkLegalDataProtectionContentText() {
    	
    	
    }
    
    public void checkLegalInformationAgainstMasterTemplate(String textFilePath) {
    	
    	LOG.info("Getting ACTUAL page content");
    	LOG.info("Waiting for page finish loading");
    	try {
			Thread.sleep(2500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		WebElement legalPageContent = driver.findElement(By.id("page-content"));
    	String fileContent = readTextFile(textFilePath);
    	LOG.info("Checking if Actual content matches master template");
    	
    	if ( legalPageContent.getText().equals(fileContent)) {
    		LOG.info("Returned value: true");
    		booleanHelper = true;
    	} else {
    		LOG.info("Returned value: false");
    		booleanHelper = false;
    	}
    }
	

    public boolean equalsCheckLegalInformationAgainstMasterTemplate() {
    	if ( booleanHelper == false) {
			LOG.info("Page Content is different from Master Template.");
			LOG.info("Please check Master Template in /resources/PageConetnt/LegalPages_LegalInformation_18.04.2018.txt");
    	}
    	return booleanHelper;
    }
    
	public boolean equalsCheckContact() {
		if (booleanHelper == false) {
			LOG.info("Some field changed, expected: ");
			System.out.println(title);
			System.out.println(subTitle);
			System.out.println(street);
			System.out.println(postCode);
			System.out.println(phone);
		}
		return booleanHelper;
	}
	
	public boolean equalsCheckImportantLinks() {
		if (booleanHelper == false) {
			LOG.info("Some field changed, expected: ");
			System.out.println(dataProtection);
			System.out.println(legalInfo);
		}
		return booleanHelper;
	}

	public boolean equalsCheckDataProtectionContent() {
		if (booleanHelper == false) {
			LOG.info("Some field changed, exptected: ");
			System.out.println(contactName);
			System.out.println(contactJobtitle);
			System.out.println(contactWorkphone);
			System.out.println(contactWorkfax);
			System.out.println(contactEmail);
			System.out.println(contactRoomnumber);
			
			LOG.info("Content titles: ");
			System.out.println(googleAnalytics);
		}
		return booleanHelper;
	}
	
	
	public LegalPages(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}


}
