package com.eahjena.pageobject;


import org.openqa.selenium.WebDriver;



public class LocatorsDepartments extends PageObject {
	
	public LocatorsDepartments(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
		public static String BW = "Betriebswirtschaft";
		public static String ETundIT = "Elektrotechnik und Informationstechnik";
		public static String GesUndPf = "Gesundheit und Pflege";
		public static String Grundl = "Grundlagenwissenschaften";
		public static String MA = "Maschinenbau";
		public static String MedinUndBio = "Medizintechnik und Biotechnologie";
		public static String SciTec = "SciTec";
		public static String Sozi = "Sozialwesen";
		public static String Wi = "Wirtschaftsingenieurwesen";
		
		//URL�s of all departments at the current status of test creation
		public static String LinkBetriebswirtschaft = "http://www.bw.eah-jena.de/";
		public static String LinkElektroUndInformationstechnik = "http://www.et.eah-jena.de/";
		public static String LinkGesundheitUndPflege = "http://www.gp.eah-jena.de/";
		public static String LinkGrundlagenwissenschaften = "http://www.gw.eah-jena.de/";
		public static String LinkMaschinenbau = "http://www.mb.eah-jena.de/";
		public static String LinkMedizinUndBiotechnologie = "http://www.mt.eah-jena.de/";
		public static String LinkSciTec = "http://www.scitec.eah-jena.de/";
		public static String LinkSozialwesen = "http://www.sw.eah-jena.de/";
		public static String LinkWirtschaftsingenieurwesen = "http://www.wi.eah-jena.de/";
		
	
}