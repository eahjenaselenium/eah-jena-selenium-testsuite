package com.eahjena.pageobject;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;


public class Views extends PageObject {
	
	@FindBy(id="showGlobalSideMainMenu")
	private WebElement navigationButton;
	
	@FindBy(id="gloabl-side-menu-wrapper")
	private WebElement navigation;
	
	@FindBy(css=".eah-collapse")
	private List<WebElement> navigationSublinks;
	
	@FindBy(css="#carousel-home .carousel-indicators li")
	private List<WebElement> headerSliderTrigger;
	
	@FindBy(css=".carousel-newsinfo .carousel-indicators li")
	private List<WebElement> newsInfoSlider;
	
	@FindBy(css=".ion-plus-round")
	private List<WebElement> infoContainer;
	
	@FindBy(css=".eahTile ul li")
	private List<WebElement> infoContainerSublinks;
	
	@FindBy(className="back-to-top")
	private WebElement backToTopButton;
	
	@FindBy(tagName="footer")
	private WebElement pageBottom;
	
	@FindBy(className="global-header-menu")
	private WebElement pageTop;
	

	
	private boolean reachedEnd;

	public WebElement getNavigation() {
		return navigation;
	}
	
	public boolean getReachedEnd() {
		return reachedEnd;
	}
	
	public void showNavigation() {
		LOG.info("click on navigation button");
		navigationButton.click();
	}

	public void showSublinks() throws InterruptedException {
		for(int i = 0; i < navigationSublinks.size(); i++) {
			LOG.info("waiting for animation");
			Thread.sleep(1000);	
			if (!reachedEnd) {
				LOG.info("selecting next possible openable sublink " + i);
			}
			
			if (navigationSublinks.get(i).isDisplayed() && navigationSublinks.get(i).isEnabled()) {
				navigationSublinks.get(i).click();
			} else {
 
				navigationSublinks.get(i).click();
			}
			
			LOG.info("successfully opened sublink");
			if(i == navigationSublinks.size() - 1) {
				LOG.info("reached end of object");
				this.reachedEnd = true;
			}
		}
	}
	
	public void triggerNewsInfoSlider() throws InterruptedException {
		for(int i = 0; i < newsInfoSlider.size(); i++) {
			LOG.info("waiting for animation");
			Thread.sleep(1000);	
			LOG.info("selecting next possible navigation info slider " + i);
			newsInfoSlider.get(i).click();
			LOG.info("successfully triggered slider");
			if(i == newsInfoSlider.size() - 1) {
				LOG.info("reached end of object");
				this.reachedEnd = true;
			}
		}
	}
	
	public void triggerHeaderSlider() throws InterruptedException {
		for(int i = 0; i < headerSliderTrigger.size(); i++) {
			LOG.info("waiting for animation");
			Thread.sleep(1000);	
			LOG.info("selecting next possible navigation slider " + i);
			headerSliderTrigger.get(i).click();
			LOG.info("successfully triggered slider");
			if(i == headerSliderTrigger.size() - 1) {
				LOG.info("reached end of object");
				this.reachedEnd = true;
			}
		}
	}
	
	public void triggerInfoContainer() throws InterruptedException {
		for(int i = 0; i < infoContainer.size(); i++) {
			LOG.info("waiting for animation");
			Thread.sleep(1000);	
			LOG.info("selecting next possible navigation Information Container Icon " + i);
			infoContainer.get(i).click();
			LOG.info("successfully triggered Container Icon");
		}
	}

	
	public void hoverInfoContainerSublinks() throws InterruptedException {
		for(int y = 0; y < infoContainerSublinks.size(); y++) {
			Actions builder = new Actions(driver);
			
			LOG.info("waiting for animation");
			Thread.sleep(500);
			LOG.info("hovering over InfoCotainer Sublink " + infoContainerSublinks.get(y).getText());
			builder.moveToElement(infoContainerSublinks.get(y)).build().perform();
			
			if(y == infoContainerSublinks.size() - 1) {
				LOG.info("reached end of object");
				this.reachedEnd = true;
			}
		}
	}

	public void scrollToBottom() throws InterruptedException {
		LOG.info("scrolling all the way down");
		pageBottom.click();
		LOG.info("waiting for Page Position to change");
		Thread.sleep(1000);
	}
	
	public void selectBackToTopButton() throws InterruptedException {
		
		LOG.info("selecting Back to Top - Button");
		backToTopButton.click();
		LOG.info("waiting for Page Position to change");
		Thread.sleep(2000);
		LOG.info("verifying that page reached start position");

		if(pageTop.isDisplayed()) {
			this.reachedEnd = true;
		}
		
	}
	
	
	
	public Views(WebDriver driver) {
		super(driver);
	}
}
