package com.eahjena.pageobject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class PageObject {	
	protected WebDriver driver;
	protected static Logger LOG = null;
	
	public void activateLogger() {
		LOG = LogManager.getLogger();		
	}
	
	public PageObject(WebDriver driver){
		activateLogger();
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
}