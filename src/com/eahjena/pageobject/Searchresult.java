package com.eahjena.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Searchresult extends PageObject {
	
	@FindBy(className="ms-srch-item-link")
	private WebElement searchItemTitle;
	
	@FindBy(id="NoResult")
	private WebElement noResultField;

	public WebElement getSearchItemTitle() {
		return searchItemTitle;
	}
	
	public boolean noResultFound() {
		// Wenn Feld "Kein Ergebnis" angezeigt wird, dann result -> true
		boolean result = noResultField.isDisplayed();
		return result;
	}
	public Searchresult(WebDriver driver) {
		super(driver);
	}
}
